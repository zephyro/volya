

$('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.page0').toggleClass('nav_open');
});


$(".btn-modal").fancybox({
    'padding'    : 0
});


$('.btn-city').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.city-inner').find('.cityModal').toggleClass('open');
});


$('.mobileNav__primary_toggle').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.mobileNav__primary').toggleClass('open');
});

$('.mobileNav__dropdown a i').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('li').toggleClass('open');
});





$(function(){
    $('.sideNav__dropdown').on({
        mouseenter: function(){

            $(this).closest('.sideNav').addClass('dropdown-open');

        },
        mouseleave: function(){
            $(this).closest('.sideNav').removeClass('dropdown-open');
        }
    });
});



$('.mainSlider').slick({
    dots: true,
    infinite: true,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><i class="fa fa-angle-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fa fa-angle-right"></i></span>',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                dots: false
            }
        }
    ]
});